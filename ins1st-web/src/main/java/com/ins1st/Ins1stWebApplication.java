package com.ins1st;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@MapperScan("com.ins1st")
@EnableTransactionManagement
public class Ins1stWebApplication {


    public static void main(String[] args) {
        SpringApplication.run(Ins1stWebApplication.class, args);
    }

}
