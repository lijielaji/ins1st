package com.ins1st.modules.sys.service;

import com.ins1st.modules.sys.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和角色表 服务类
 * </p>
 *
 * @author sun
 * @since 2019-05-09
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
