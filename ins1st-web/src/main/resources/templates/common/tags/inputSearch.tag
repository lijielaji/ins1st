@/*
标签参数 -
id : input框id
name: input框name
inputName ： 名称
@*/
<div class="layui-inline layui-show-xs-block">
    <input class="layui-input" type="text"  autocomplete="off" placeholder="${inputName}" name="${name}" id="${id}">
</div>